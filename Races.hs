module Races where

class Race x where
  name :: x -> String
  desc :: x -> String
