{-# LANGUAGE CPP #-}

#include "top.hs"

module MyPrelude where

#include "hide.hs"

import Numeric.Natural

import System.Process (system)


clear = case system "clear" of
          e -> putStrLn "\ESC[2J"

-- So that myshow s == s for any s :: String (it does not add quotes unlike show)
class MyShow a where
  myshow :: a -> String

instance {-# OVERLAPPING #-} MyShow String where
  myshow = id

instance Show x => MyShow x where
  myshow = show

type Nat = Natural
type Str = String

(!!) :: [a] -> Nat -> a
(!!) = genericIndex

length :: Foldable t => t a -> Nat
length lst = fromIntegral $ Prelude.length lst

drop :: Nat -> [a] -> [a]
drop = genericDrop

newline :: Char
newline = '\n'
