#include "top.hs"

#ifdef MODULE
module MODULE where
#endif

#include "hide.hs"

#define INFO(f) (f __FILE__ __LINE__)

#ifndef NDEBUG
import Debug.Trace
#else
import FakeTrace
#endif

import MyPrelude
