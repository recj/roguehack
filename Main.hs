#!/usr/bin/env runhaskell

{-# LANGUAGE CPP #-}

#define MODULE Main
#include "header.hs"

import Menu

import Moving
import Entity

room1 = RoomC
  {
    name  = "First Room",
    width = 5,
    blocks = [Wall, Wall, Wall, Wall, Wall,
              Wall, Empt, Empt, Empt, Wall,
              Wall, Empt, Wall, Empt, Wall,
              Wall, Empt, Wall, Empt, Wall,
              Wall, Empt, Empt, Empt, Wall,
              Wall, Wall, Wall, Wall, Wall],
    entities = [(6, '@'),
                (7, sym Wall)]
  }

main :: IO ()
main
  = do res <- menu "Select your race" True ["Yerles"
                                           ,"Fairy"
                                           ,"Dwarf"
                                           ,"Elea"
                                           ,"Snail"
                                           ,"Putit"
                                           ,"Undead Lich"
                                           ,"Golem"
                                           ,"Child of \"god\""
                                           ,"Mutant"
                                           ,"Machine"
                                           ,"Nekomata"
                                           ,"Spirggan"
                                           ,"Tengu"
                                           ,"Orge"
                                           ,"Vine Stalker"
                                           ,"Naga"
                                           ,"Formicid"
                                           ,"Kobold"
                                           ,"Octapode"
                                           ,"Draconian"]
       loop room1
