{-# LANGUAGE CPP #-}

#include "top.hs"

module Moving (Room (RoomC), Block (Empt, Wall), loop, name, width, blocks, entities, sym) where

#include "header.hs"

import Entity

import System.IO hiding (print)

data Key = KUp
         | KDown
         | KRight
         | KLeft
         | KUpLeft
         | KUpRight
         | KDownLeft
         | KDownRight

data Block = Empt
           | Wall

sym :: Block -> Char
sym Empt = ' '
sym Wall = '#'

data Room = RoomC
  {
    name     :: String,
    width    :: Nat,
    blocks   :: [Block],
    entities :: [Entity]
  }

display e a pos = case lookup pos e of
                    Nothing -> sym a
                    Just a  -> a

instance Show Room where
  show (RoomC name width blocks entities) = print 0 ("Room: " ++ name ++ "\n") width blocks entities
    where print :: Nat -> String -> Nat -> [Block] -> [Entity] -> String
          print pos r w (a:as) e = print (pos + 1) (r ++ ((display e a pos):(case pos `mod` w == w - 1 of
                                                                               True -> "\n"
                                                                               _    -> ""))) w as e
          print _   r _ []     _  = r

process :: Room -> IO Room
process room = do entities2 <- process2 (width room) (blocks room) [] (entities room)
                  return $ room { entities = entities2 }

process2 :: Nat -> [Block] -> [Entity] -> [Entity] -> IO [Entity]
process2 width blocks r ((pos, '@'):xs) = do input <- getInput
                                             let pos2 = case input of
                                                          KUp        -> pos - width
                                                          KDown      -> pos + width
                                                          KLeft      -> pos - 1
                                                          KRight     -> pos + 1
                                                          KUpLeft    -> pos - width - 1
                                                          KUpRight   -> pos - width + 1
                                                          KDownLeft  -> pos + width - 1
                                                          KDownRight -> pos + width + 1 in
                                               process2 width blocks (r ++ [((case blocks !! pos2 of
                                                                                Empt -> case lookup pos2 (r ++ xs) of
                                                                                          Nothing -> pos2
                                                                                          _       -> pos2 -- TODO
                                                                                Wall -> pos), '@')]) xs

process2 width blocks r (x:xs)          = process2 width blocks (r ++ [x]) xs
process2 _     _      r []              = return r

getInput :: IO Key
getInput = do hSetEcho stdin False
              hSetBuffering stdin NoBuffering
              c <- getChar
              case c of
                '8' -> return KUp
                '2' -> return KDown
                '4' -> return KLeft
                '6' -> return KRight
                '7' -> return KUpLeft
                '9' -> return KUpRight
                '1' -> return KDownLeft
                '3' -> return KDownRight
                '\ESC' -> do c <- getChar
                             case c of
                               '[' -> do c <- getChar
                                         case c of
                                           'D' -> return KLeft
                                           'C' -> return KRight
                                           'A' -> return KUp
                                           'B' -> return KDown
                _   -> do putStrLn "Try again!"
                          getInput

print :: Room -> IO ()
print room = do clear
                putStr $ show room


loop :: Room -> IO ()
loop room  = do print room
                process room >>= loop

-- geti w (x, y) = y * w + x
