#!/usr/bin/env runhaskell

{-# LANGUAGE CPP #-}

#define MODULE Entity
#include "header.hs"

type Entity = (Nat, Char)
